<?php

namespace Database\Factories;

use App\Models\ToDo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ToDoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = ToDo::class;

    public function definition(): array
    {
        $name = $this->faker->name;
        $description = $this->faker->text;

        return [
            'task' => "Task ".$name,
            'description' => $description,
            'status' => 'done',
        ];
    }
}
