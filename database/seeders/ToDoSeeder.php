<?php

namespace Database\Seeders;

use App\Models\ToDo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ToDoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 100; $i++) {
            $input = [
                "task"=>"Task ".$i,
                "description"=>"Description task ".$i,
                "status"=> $i % 2 == 0 ? "done":"pending"
            ];
            ToDo::create($input);
        }
    }
}
