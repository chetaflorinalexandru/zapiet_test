<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('to-do/list', [\App\Http\Controllers\ToDo::class, 'list']);
Route::post('to-do/updateStatus/{id}', [\App\Http\Controllers\ToDo::class, 'updateStatus']);
Route::get('to-do/updateAllStatuses', [\App\Http\Controllers\ToDo::class, 'updateAllStatuses']);
