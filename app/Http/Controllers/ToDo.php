<?php

namespace App\Http\Controllers;

use App\Http\Requests\ToDoChangeStatusRequest;
use App\Jobs\ChangeToDoStatuses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ToDo extends Controller
{
    public function list(Request $request)
    {

        $perPage = 10;
        if ($request->has('per_page')) $perPage = $request->per_page;

        $response['status_code'] = 200;
        $toDos = \App\Models\ToDo::paginate($perPage);
        $response['response'] = $toDos;

        return Response::json($response);
    }

    public function updateStatus(ToDoChangeStatusRequest $request, $id)
    {
        \App\Models\ToDo::whereId($id)->update($request->all());
        return Response::json(['status_code'=>200,"response"=>["id"=>$id]]);
    }

    public function updateAllStatuses(Request $request)
    {
        dispatch(new ChangeToDoStatuses());
        return Response::json(['status_code'=>200,"response"=>["count"=>\App\Models\ToDo::count()]]);
    }

}
