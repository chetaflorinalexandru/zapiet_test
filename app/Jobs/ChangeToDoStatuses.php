<?php

namespace App\Jobs;

use App\Models\ToDo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ChangeToDoStatuses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        try {
            DB::beginTransaction();
            $toDos = \App\Models\ToDo::where('status','pending')->get();
            foreach ($toDos as $todo){
                $todo->status = "done";
                $todo->save();
            }
            DB::commit();
        }catch (HttpResponseException $exception){
            Log::error($exception);
            DB::rollBack();
        }
    }
}
