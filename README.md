Docker setup:\
Step 1 : run ``docker compose build``\
Step 2 : Create `.env` file and copy the the content from `.env.example`\
Step 3 : Go to app container terminal and run ``composer install && php artisan migrate``\

Endpoints:\
    * Endpoint list to do items  => [GET method] `localhost:80/api/to-do/list`\
    * Endpoint update status to do item  => [POST method] `localhost:80/api/to-do/updateStatus/{id}`\
    * Endpoint update all to dos using queue laravel system  => [GET method] `localhost:80/api/to-do/updateAllStatuses`\

Important:\
    * To start queue procces you have to go in app container terminal and run ``php artisan queue:work``
