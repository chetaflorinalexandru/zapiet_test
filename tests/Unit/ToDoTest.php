<?php

namespace Tests\Unit;

use App\Models\ToDo;
use Tests\TestCase;
use function PHPUnit\Framework\assertJson;

class ToDoTest extends TestCase
{
    public function test_to_do_list_endpoint(): void
    {
        $response = $this->json('GET','/api/to-do/list');
        $response->assertStatus(200)
            ->assertSee('status_code')
            ->assertSee('Next')
            ->assertSee('Previous');
    }

    public function test_to_do_update_status_endpoint(): void
    {
        $fake_toDo = ToDo::factory()->create();
        $response = $this->json('POST','/api/to-do/updateStatus/'.$fake_toDo->id,["status"=>"pending"]);

        $response->assertStatus(200)
            ->assertSee('status_code')
            ->assertJson(
                [
                    "status_code"=>200,
                    "response"=>[
                        "id"=>$fake_toDo->id
                    ]
                ]
            );
    }
}


#./vendor/bin/phpunit
